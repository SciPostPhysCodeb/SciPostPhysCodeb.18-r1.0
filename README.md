# Codebase release 1.0 for GRIFFIN

by Lisong Chen, Ayres Freitas

SciPost Phys. Codebases 18-r1.0 (2023) - published 2023-10-10

[DOI:10.21468/SciPostPhysCodeb.18-r1.0](https://doi.org/10.21468/SciPostPhysCodeb.18-r1.0)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.18-r1.0) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Lisong Chen, Ayres Freitas.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Live (external) repository at [https://github.com/lisongc/GRIFFIN/releases/tag/v1.0.0](https://github.com/lisongc/GRIFFIN/releases/tag/v1.0.0)
* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.18-r1.0](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.18-r1.0)
