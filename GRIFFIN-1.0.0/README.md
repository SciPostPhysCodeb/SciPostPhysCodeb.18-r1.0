# GRIFFIN  
GRIFFIN (Gauge-invariant Resonance In Four-Fermion INteractions), as an object-oriented C++ library for electroweak radiative corrections, provides a modular framework of describing 2-to-2 fermion scattering processes with specific attention to a consistent gauge-invariant description of the gauge-boson resonance. Version 1.0 of this library provides Standard Model predictions for the 2-to-2 fermion processes with full NNLO and leading higher-order contributions on the Z-resonance.

# Download and Installation
One can either clone or download the repository. 
To run and test the program, please run `make testmatel` to compile the testprogram.
There are three test programs: `testmatel`, `testdeltar`, and `testtools`. One can switch the option by 
changing the `MAIN` in the `makefile`.

# Manual
GRIFFIN v1.0 is available and can be found [here](https://github.com/lisongc/GRIFFIN_manual)

# Using GRIFFIN in your research
The paper will be on ArXiv soon.

# License
The authors of this public repository give the consent to the external users to use, reproduce, fork, and distribute the content, but with careful citations.

# Contacts
Ayres Freitas : afreitas@pitt.edu
Lisong Chen : lisong.chen@kit.edu
